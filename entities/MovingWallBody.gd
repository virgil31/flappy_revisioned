extends StaticBody2D

const SPEED = 2
const UP_DOWN_SPEED = 0.5

var framesLapsed = 0

# Funzione chiamata per ogni frame (60 volte al secondo)
func _physics_process(delta):	
	# Il muro si sposta sempre verso sinistra
	position.x -= SPEED
	
	# Il muro si sposta in alto e in basso alternando ogni secondo
	framesLapsed += 1
	position.y += UP_DOWN_SPEED if (framesLapsed/60%2 == 0) else -UP_DOWN_SPEED

extends KinematicBody2D

const GRAVITY = 10
const FLAP = 200
var motion = Vector2()


# Funzione chiamata per ogni frame (60 volte al secondo)
func _physics_process(delta):
	# Applichiamo costantemente al giocatore una forza che lo spinge verso il basso
	motion.y += GRAVITY
	
	# Se abbiamo premuto lo SPAZIO (comando FLAP) allora il giocatore si sposta in ALTO
	if (Input.is_action_just_pressed("FLAP")):
		motion.y = -FLAP
		
	# Muovo il nostro body
	move_and_slide(motion)



func _on_Area2D_body_entered(body):
	# Quando colpisco un WALL...
	if (body.name == "WallBody" || body.name == "MovingWallBody"):
		# ...salvo nel caso il nuovo record
		if (Globals.score > Globals.bestScore):
			saveBestScore()
		
		# ...e resetto lo score attuale e ricarico la scena
		Globals.score = 0
		get_tree().reload_current_scene()


# Funzione per salvare su file il record assoluto raggiunto
func saveBestScore():
	Globals.bestScore = Globals.score
	var file = File.new()
	file.open("res://save_game.dat", File.WRITE)
	file.store_string(str(Globals.bestScore))
	file.close()



func _on_Area2D_area_entered(area):
	# Quando entro nella POINT AREA...
	if (area.name == "PointArea"):
		# ...incremento lo score
		Globals.score += 1
		
		# ...e se ho superato il record attivo le fiamme!
		if (Globals.score > Globals.bestScore):
			get_node("OnFireAnimation").show()





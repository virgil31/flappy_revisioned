extends StaticBody2D

const SPEED = 2

# Funzione chiamata per ogni frame (60 volte al secondo)
func _physics_process(delta):	
	# Il muro si sposta sempre verso sinistra
	position.x -= SPEED

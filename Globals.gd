extends Node

# Essendo specificata nell'AutoLoad del progetto, questa classe è accessibile GLOBALMENTE
var score :int
var bestScore :int


# All'avvio leggo dal salvataggio il *bestScore*
func _ready():
	var file = File.new()
	file.open("res://save_game.dat", File.READ)
	bestScore = int(file.get_as_text())
	file.close()

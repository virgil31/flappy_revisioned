extends Node2D

# Quando premo il pulsante START cambio scena
func _on_Button_pressed():
	get_tree().change_scene("res://scenarios/WorldNode.tscn")

# Quando carico il menu riporto l'attuale BEST SCORE
func _ready():
	get_node("ScoreLabel").text = "Best Score: " + str(Globals.bestScore)

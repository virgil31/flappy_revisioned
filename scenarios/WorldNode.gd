extends Node2D

const WALL_NODE = preload("res://entities/WallNode.tscn")
const MOVING_WALL_NODE = preload("res://entities/MovingWallNode.tscn")


func _on_ResetWallArea_area_entered(area):
	# Quando una POINT AREA finisce nell'area del nostro RESETTER...
	if (area.name == "PointArea"):
		# ...ottengo il muro di riferimento
		var body = area.get_parent() as StaticBody2D
		# ...lo rimuovo dalla scena
		body.queue_free()
		# ...creo un nuovo muro impostandone la posizione SEMI-casuale
		var newWall = WALL_NODE.instance() if (body.name == "WallBody") else MOVING_WALL_NODE.instance()
		newWall.position = Vector2(350, rand_range(-55, 55))
		# ...aggiungo ALLA FINE DEL FRAME ATTUALE il nuovo muro alla scena
		call_deferred("add_child", newWall)



# Funzione chiamata per ogni frame (60 volte al secondo)
func _physics_process(delta):
	# Tengo aggiornata la label con gli scores
	get_node("LabelNode/Label").text = " Best: " + str(Globals.bestScore) + " - Score: " + str(Globals.score)

# Quando premo il pulsante BACK ritorno al menu iniziale
func _on_BackButton_pressed():
	get_tree().change_scene("res://scenarios/StartMenuNode.tscn")



		
		
